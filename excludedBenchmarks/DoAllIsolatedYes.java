package benchmarks;
import static permission.PermissionChecks.*;
/*
   A translation of: doall1-orig-no.c
   Originally produced at the Lawrence Livermore National Laboratory
   Written by Chunhua Liao, Pei-Hung Lin, Joshua Asplund,
   Markus Schordan, and Ian Karlin

   Translated at Brigham Young University by Kyle Storey

 */

import static edu.rice.hj.Module2.launchHabaneroApp;
import static edu.rice.hj.Module2.forAll;
import static edu.rice.hj.Module2.isolated;
import edu.rice.hj.api.*;

public class DoAllIsolatedYes {
    private static final int loops = 10;
    static int[] a = new int[loops];
    public static void main(String[] args) throws SuspendableException {
        launchHabaneroApp(new HjSuspendable() {

                @Override
                public void run() throws SuspendableException {

                forAll(0, loops, new HjSuspendingProcedure<Integer>() {
                        public void apply(Integer i) throws SuspendableException {
                        final int index = i;
                        if (index != loops) {
                            isolated(new HjRunnable() {
                                public void run() {
                                    System.out.println("Isolated: " + index);
                                    acquireW(a, index);
                                    a[index] = a[index]+1;
                                    releaseW(a, index);
                                }	
                            });	
                        }
                        else {
                            isolated(new HjRunnable() {
                                public void run() {
                                    System.out.println("Isolated: " + index);
                                }	
                            });	
                            if (a[0] == 0) System.out.println("Race Case Run");
                        }
                }
                        });
                }

        });
    }
}
