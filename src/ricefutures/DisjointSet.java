package ricefutures;

import java.lang.NullPointerException;
import java.util.Map;
import java.util.HashMap;

/**
 * Simple implementation of the union find data structure
 */
public class DisjointSet {
    Map<Integer, Integer> parent;

    public DisjointSet() {
      this.parent = new HashMap<>();
    }

    public DisjointSet(DisjointSet sets) {
      this.parent = new HashMap<>(sets.parent);
    }

    public void makeSet(Integer i) {
        parent.put(i, i);
    }

    public int find(int i) {
        if (parent.get(i) == null) throw new NullPointerException("Disjoint Set:makeset() " + i + " was null");
        if (parent.get(i) == i) {
            return i;
        }
        int result = find(parent.get(i));
        parent.put(i, result);
        return result;
    }

    public void union(int i, int j) {
        if (parent.get(i) == null) throw new NullPointerException("Disjoint Set:union() " + i + " was null");
        parent.put(j, i);
    }
}
