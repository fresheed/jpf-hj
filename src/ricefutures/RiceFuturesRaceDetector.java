package ricefutures;

import util.Search;
import gov.nasa.jpf.JPF;
import gov.nasa.jpf.Config;

public class RiceFuturesRaceDetector extends Search {
  public RiceFuturesRaceDetector(Config conf, JPF jpf) {
    super(conf, jpf, new FuturesDetector());
  }
}
