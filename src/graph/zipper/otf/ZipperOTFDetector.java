package graph.zipper.otf;

import graph.zipper.otf.node.IsolatedNode;
import util.AddableChoiceGenerator;
import util.Detector;

import java.util.HashMap;
import java.util.Map;

public class ZipperOTFDetector extends Detector {
  private ZipperOTF state = new ZipperOTF(0);

  @Override
  public void setUseDC(boolean useDC) {
    state.useDC = useDC;
  }

  @Override
  public void handleAcquire(int tid) {
    if (!this.state.hasRace())
      state.isolateStart(tid);
  }

  @Override
  public void handleRelease(int tid, AddableChoiceGenerator cg) {
    if (!this.state.hasRace())
      cg.putObject(state.isolateEnd(tid));
  }

  @Override
  public void handleRead(int tid, String uniqueLabel) {
    if (!this.state.hasRace())
      state.read(tid, uniqueLabel);
  }

  @Override
  public void handleWrite(int tid, String uniqueLabel) {
    if (!this.state.hasRace())
      state.write(tid, uniqueLabel);
  }

  @Override
  public void handleFork(int parent, int child, boolean future) {
    if (!this.state.hasRace())
      state.fork(parent, child);
  }

  @Override
  public void handleJoin(int parent, int child, boolean finish) {
    if (!this.state.hasRace())
      state.join(parent, child);
  }

  @Override
  public void handleHalt(String programName) {
    if(debug) {
      state.halt(programName);
    }
  }

  @Override
  public boolean isDependent(AddableChoiceGenerator cg1, AddableChoiceGenerator cg2) {
    if (cg1 == cg2 || !cg1.isIsolateCG() || !cg2.isIsolateCG()) {
      return false;
    } else {
      IsolatedNode iso1 = (IsolatedNode) cg1.getObject();
      IsolatedNode iso2 = (IsolatedNode) cg2.getObject();
      return iso1.incoming.contains(iso2) || iso2.incoming.contains(iso1);
    }
  }

  @Override
  public void resetState(Object state) {
    this.state = ZipperOTF.resetTo((ZipperOTF) state);
  }

  @Override
  public Object getImmutableState() {
    return state.deepCopy();
  }

  @Override
  public boolean hasRace() {
    return this.state.hasRace();
  }
}
