package graph.zipper.otf.node;

import java.util.Map;

public class JoinNode extends ZipperNode {
  public JoinNode(int tid, Map<Integer, ZipperNode> sync) {
    super(tid, sync);
  }
}
