package graph.zipper.otf.node;

import graph.util.ArrayAccess;
import graph.util.ObjectAccess;

import java.util.*;

public class IsolatedNode extends RaceyNode {
  public Set<IsolatedNode> incoming = new HashSet<>();

  public IsolatedNode(int tid, Map<Integer, ZipperNode> sync) {
    super(tid, sync);
  }

  public void isolatedEdge(IsolatedNode node) {
    incoming.add(node);
    for(Map.Entry<Integer, ZipperNode> entry : node.sync.entrySet()) {
      if(!sync.containsKey(entry.getKey()) || sync.get(entry.getKey()).id < entry.getValue().id) {
        sync.put(entry.getKey(), entry.getValue());
      }
    }
    sync.put(node.tid, node.child);
  }

  @Override
  public String toString() {
    return super.toString() + "*";
  }
}
