package graph.zipper.otf;

import graph.zipper.otf.node.ForkNode;
import graph.zipper.otf.node.IsolatedNode;
import graph.zipper.otf.node.JoinNode;
import graph.zipper.otf.node.ZipperNode;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class DotWriter {
  private static int graphCount = 0;
  private static Set<ZipperNode> done;

  public static void createDot(ZipperNode node, String filename) {
    done = new HashSet();
    try {
      BufferedWriter writer = new BufferedWriter(new FileWriter("dots/" + filename + ++graphCount + ".dot"));
      writer.write("digraph { \n");
      appendToDot(findRoot(node), writer);
      writer.append("}");
      writer.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static void appendToDot(ZipperNode node, BufferedWriter writer) throws IOException {
    if(node == null /*|| done.contains(node)*/) {
      return;
    }
    if (node instanceof ForkNode) {
      writer.append(String.format("\"%s\" [shape=triangle];\n", node));
      appendToDot(((ForkNode)node).forked, writer);
      writeEdge(node, ((ForkNode)node).forked, false, writer);
    } else if (node instanceof IsolatedNode) {
      writer.append(String.format("\"%s\" [shape=box color=green];\n", node));
      for (IsolatedNode incoming : ((IsolatedNode) node).incoming) {
        writeEdge(incoming, node, true, writer);
      }
    } else if (node instanceof JoinNode) {
      writer.append(String.format("\"%s\" [shape=invtriangle];\n", node));
    }
    else if(node.error){
      writer.append(String.format("\"%s\" [color=red];\n", node));
    }
    appendToDot(node.child, writer);
    writeEdge(node, node.child, false, writer);
  }

  private static void writeEdge(ZipperNode a, ZipperNode b, boolean dotted, BufferedWriter writer) throws IOException {
    if(a != null && b != null && !done.contains(a) && !done.contains(b)) {
      writer.append(String.format("\"%s\" -> \"%s\"%s;\n", a, b, dotted ? " [style=dotted color=green]" : ""));
      if(a instanceof ForkNode && ((ForkNode) a).forked == b) {
        done.add(((ForkNode) a).forked);
      } else {
        done.add(a.child);
      }
    }
  }

  private static ZipperNode findRoot(ZipperNode node) {
      if (node.parent == null) {
      return node;
    } else {
      return findRoot(node.parent);
    }
  }
}
