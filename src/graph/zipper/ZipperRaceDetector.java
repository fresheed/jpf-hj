package graph.zipper;

import util.Search;
import graph.util.Graph;
import graph.util.Node;
import gov.nasa.jpf.JPF;
import gov.nasa.jpf.Config;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.VM;

import java.util.List;

public class ZipperRaceDetector extends Search {
  int count = 0;
  boolean race = false;
  int maxTasks = 0;
  ZipperCheck checker = new ZipperCheck();

  public ZipperRaceDetector(Config conf, JPF jpf) {
    super(conf, jpf, new ZipperDetector());
  }

  @Override
  public void objectReleased(VM vm, ThreadInfo ti, ElementInfo ei) {
    ZipperDetector t = (ZipperDetector) detector;
    if (ei.toString().startsWith("hj.runtime.wsh.SuspendableActivity")) {
      System.out.println("Writing Graph " + (++count));
      t.writeGraph("./dots/" + vm.getSUTName() + "-" + count + ".dot");
      if (t.getTaskCount() > maxTasks)
        maxTasks = t.getTaskCount();
      Graph graph = t.getGraph();
      List<Node> isolationOrder = t.getIsolationOrder();
      race = checker.check(graph, isolationOrder);
    }
  }

  @Override
  public String getErrorMessage () {
    return race ? "Data race detected" : null;
  }

  @Override
  public boolean check(gov.nasa.jpf.search.Search search, VM vm) {
    return !race;
  }

  @Override
  public void searchFinished(gov.nasa.jpf.search.Search search) {
    super.searchFinished(search);
    System.out.println("Tasks: " + maxTasks);
  }

}
