package graph.util;

import org.jgraph.graph.DefaultEdge;

/**
 * Created by fresheed on 11.06.19.
 */
public class ConditionalEdge extends DefaultEdge {
    public String condition;

    public void setCondition(String condition){
        this.condition = condition;
    }

    @Override
    public String toString(){
        return condition;
    }
}
