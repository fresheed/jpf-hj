package graph.symbolic;

import gov.nasa.jpf.symbc.concolic.PCAnalyzer;
import gov.nasa.jpf.symbc.numeric.LogicalORLinearIntegerConstraints;
import gov.nasa.jpf.symbc.numeric.PCParser;
import gov.nasa.jpf.symbc.numeric.PathCondition;
import gov.nasa.jpf.symbc.numeric.SymbolicConstraintsGeneral;
import graph.util.ArrayAccess;
import graph.util.ObjectAccess;

import java.nio.file.Path;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * A node in symbolic graph. May contain accesses from different threads and symbolic execution branches.
 */
public class SymbolicNode {
  public enum NodeType { ASYNC, JOIN, ACTIVITY, ISOLATED }
  static int count = 0;

  private NodeType type;
  private final Map<String, SymbolicAccessHistory> objAccesses = new HashMap<>();
  private final Map<PathCondition, List<ArrayAccess>> arrAccesses = new HashMap<>();
  private int index;
  public boolean error = false;

  private SymbolicNode(NodeType type) {
    this.type = type;
    this.index = count++;
  }

  public static SymbolicNode mkForkNode() {
    return new SymbolicNode(NodeType.ASYNC);
  }

  public static SymbolicNode mkJoinNode() {
    return new SymbolicNode(NodeType.JOIN);
  }

  public static SymbolicNode mkActivityNode() {
    return new SymbolicNode(NodeType.ACTIVITY);
  }

  public static SymbolicNode mkIsolatedNode() {
    return new SymbolicNode(NodeType.ISOLATED);
  }

  private String arrLabel(String label) {
    return label.substring(0, label.indexOf('['));
  }

  private int arrIndex(String label) {
    int i = label.indexOf('[');
    return Integer.parseInt(label.substring(i + 1, label.length() - 1));
  }

  public void addAccess(String label, boolean isWrite, PathCondition pc) {
    if (label.startsWith("util.array@")) {
      String k = arrLabel(label);
      int i = arrIndex(label);
      arrAccesses.putIfAbsent(pc, new ArrayList<>());
      arrAccesses.get(pc).add(new ArrayAccess(k, i, isWrite));
    } else {
      SymbolicAccessHistory history = objAccesses.computeIfAbsent(label, l -> new SymbolicAccessHistory());
      if (isWrite){
        history.storeWrite(pc);
      } else {
        history.storeRead(pc);
      }
    }
  }

  public boolean isJoin() {
    return type == NodeType.JOIN;
  }

  public boolean isAsync() {
    return type == NodeType.ASYNC;
  }

  public boolean isReadWrite() {
    return objAccesses.size() > 0 || arrAccesses.size() > 0;
  }

  public boolean isIsolated() {
    return type == NodeType.ISOLATED;
  }

  public int getIndex() {
    return index;
  }

//  public Set<String> intersection(SymbolicNode n) {
//    Set<String> intersection = new HashSet<>();
//    for (String k : objAccesses.keySet()) {
//      if (n.objAccesses.containsKey(k) && objAccesses.get(k).conflicts(n.objAccesses.get(k)))
//        intersection.add(k);
//    }
//    for (String k : arrAccesses.keySet()) {
//      if (n.arrAccesses.containsKey(k) && arrAccesses.get(k).conflicts(n.arrAccesses.get(k)))
//        intersection.add(k);
//    }
//  }

  public boolean mayConflict(SymbolicNode other){
    Set<String> myLocations = objAccesses.keySet(), otherLocations = other.objAccesses.keySet();
    Set<String> sharedLocations = new HashSet<>(myLocations);
    sharedLocations.retainAll(otherLocations);
    boolean hasRace = sharedLocations.stream().anyMatch(loc ->
            objAccesses.get(loc).mayConflict(other.objAccesses.get(loc)));
    return hasRace;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(index);
    sb.append('\n');
    if (type == NodeType.ACTIVITY){
      sb.append(describeAccesses());
    }
    return sb.toString();
  }

  private String describeAccesses(){
    StringBuffer sb = new StringBuffer();
    BiFunction<String, SymbolicAccessHistory, String> locationInfo = (label, history) -> {
      String[] tokens = label.split("\\.");
      String varName = tokens[tokens.length-1];
      String condition = history.toString().replace('\n', ' ');
      return varName+": "+condition;
    };
    sb.append(objAccesses.entrySet().stream().map(it -> locationInfo.apply(it.getKey(), it.getValue()))
            .collect(Collectors.joining("\n")));
    sb.append('\n');
    //sb.append(arrAccesses.stream().map(trim).collect(Collectors.joining(", ")));
    if (!arrAccesses.isEmpty()){
      sb.append("+ some array accesses");
    }
    return sb.toString();

  }

}
