package graph.symbolic;

import gov.nasa.jpf.symbc.SymbolicInstructionFactory;
import gov.nasa.jpf.symbc.numeric.*;

import java.util.function.BiFunction;
import java.util.stream.Stream;

import static gov.nasa.jpf.symbc.numeric.Comparator.EQ;

/**
 Stores path conditions on which reads/writes occur. <br/>
 Currently only LogicalORLinearIntegerConstraints are supported since these are the only supporting OR operation
 (but even it doesn't work properly, see unsatDisjunctionSatisfiable). <br/>
 Also performs data race realizability check.
 */
public class SymbolicAccessHistory {
    // read and write conditions are separated, because read and writes may conflict to different symbolic nodes
    // it's not clear how to make a disjunction of general Constraint
    private final LogicalORLinearIntegerConstraints readConditions = new LogicalORLinearIntegerConstraints(),
            writeConditions = new LogicalORLinearIntegerConstraints();
    private static final LinearIntegerConstraint
            unsat = new LinearIntegerConstraint(new IntegerConstant(0), EQ, new IntegerConstant(1)),
            tautology = new LinearIntegerConstraint(new IntegerConstant(0), EQ, new IntegerConstant(0));

    public SymbolicAccessHistory(){
        // is there a simpler way to treat empty LogicalORLinearIntegerConstraints as false?
        readConditions.addToList(unsat);
        writeConditions.addToList(unsat);
    }

    public void storeRead(PathCondition pc){
        tryStoreAccess(pc, readConditions);
    }

    public void storeWrite(PathCondition pc){
        tryStoreAccess(pc, writeConditions);
    }

    private void tryStoreAccess(PathCondition pc, LogicalORLinearIntegerConstraints history){
        try {
            LinearIntegerConstraint constraint = (LinearIntegerConstraint)pc.header;
            if (constraint == null){ // initial PC has no constraints
                constraint = tautology;
            }
            history.addToList(constraint);
        } catch (ClassCastException e){
            throw new RuntimeException("Only linear constraints are currently supported");
        }
    }

    public boolean mayConflict(SymbolicAccessHistory other){
        // conflict: (R1 && W1) || (W1 && R2) || (W1 && W2)
        // convert each && to dnf and check one by one
        BiFunction<LogicalORLinearIntegerConstraints, LogicalORLinearIntegerConstraints, Stream<PathCondition>> combine = (l1, l2) ->
                l1.getList().stream().flatMap(c1 -> l2.getList().stream().map(c2 -> {
                    PathCondition pc = new PathCondition();
                    pc.prependUnlessRepeated(cloneConstraint(c1));
                    pc.prependUnlessRepeated(cloneConstraint(c2));
                    return pc;
                }));
        Stream<PathCondition> fullDNF = Stream.concat(
                Stream.concat(combine.apply(this.readConditions, other.writeConditions),
                        combine.apply(this.writeConditions, other.readConditions)),
                combine.apply(this.writeConditions, other.writeConditions));
        SymbolicInstructionFactory.debugMode = false; // turn off during sequence of calls
        boolean canConflict = fullDNF.anyMatch(constraint ->
            new SymbolicConstraintsGeneral().isSatisfiable(constraint)
        );
        SymbolicInstructionFactory.debugMode = true;
        return canConflict;
    }

    /**
     * Needed since PathCondition constraint addition methods modify input objects
     */
    private LinearIntegerConstraint cloneConstraint(LinearIntegerConstraint orig){
        LinearIntegerConstraint clone = new LinearIntegerConstraint(orig);
        if (orig.and != null){
            clone.and = cloneConstraint((LinearIntegerConstraint) orig.and);
        }
        return clone;
    }

    /**
     * An example of strange behavior with falsy constraint inside disjunction
     */
    private void unsatDisjunctionSatisfiable(){
        LogicalORLinearIntegerConstraints test = new LogicalORLinearIntegerConstraints();
        test.addToList(unsat);
        PathCondition pc = new PathCondition();
        pc.prependUnlessRepeated(test);
        System.out.println("Unsat solved? "+new SymbolicConstraintsGeneral().isSatisfiable(pc));
    }

    @Override
    public String toString() {
        return String.format("R: %s\nW: %s", SymbolicUtils.removePrefixes(readConditions.toString()),
                SymbolicUtils.removePrefixes(writeConditions.toString()));
    }
}
