package graph.symbolic;

import benchmarks.symbolicComputationGraph.staticGraph.RaceDetectionResult;
import gov.nasa.jpf.Config;
import gov.nasa.jpf.JPF;
import gov.nasa.jpf.search.Search;
import util.Detector;

import java.util.HashMap;
import java.util.Map;


/**
 * An extension of Search which restores state on a symbolic backtrack
 */
public abstract class GeneralSymbolicSearch extends util.Search{
  // RDS for restoring; needed by external choice generators e.g. PCC
  private Map<Integer, RaceDetectorState> statesRDSs = new HashMap<>();

  public GeneralSymbolicSearch(Config conf, JPF jpf, Detector detector) {
    super(conf, jpf, detector);
  }

  /**
   * An overkill: every state occured is saved, but in fact only those used for backtracking are reused.
   */
  @Override
  public void stateAdvanced(gov.nasa.jpf.search.Search search) {
    statesRDSs.put(search.getStateId(), new RaceDetectorState(finishBlocks, tids, detector.getImmutableState()));
    super.stateAdvanced(search);
  }

  @Override
  public void stateBacktracked(gov.nasa.jpf.search.Search search) {
    if (search.getStateId() >= 0) { // <0 occurs on search termination
      RaceDetectorState savedRDS = statesRDSs.get(search.getStateId());
      resetState(savedRDS);
      super.stateBacktracked(search);
    }
  }

  @Override
  public void searchFinished(Search search) {
    boolean shouldCheck = search.getConfig().getBoolean("debug.testBenchmarks");
    if (shouldCheck) {
      assertBenchmarkCorrect(search);
    }
  }

  /**
   * Compares current benchmarks's analysis result to the expected one.
   * The benchmark should be annotated with RaceDetectionResult.
   */
  private void assertBenchmarkCorrect(Search search){
    boolean hasRace = search.getErrors().stream().anyMatch(e ->
            e.getProperty() instanceof GeneralSymbolicSearch);
    String sut = search.getVM().getSUTName();
    try {
      RaceDetectionResult annotation = (RaceDetectionResult) Class.forName(sut).getAnnotations()[0];
      boolean expected = annotation.hasRace();
      // write to stderr because in corresponding task stdout is discarded
      if (expected == hasRace) {
        System.err.println(String.format("Benchmark %s test succeeded", sut));
      } else {
        System.err.println(String.format("Benchmark %s test failed: expected race=%b, actually=%b", sut, expected, hasRace));
      }
    } catch (ClassNotFoundException e) {
      throw new RuntimeException("An internal error");
    } catch (ClassCastException | ArrayIndexOutOfBoundsException e) {
      throw new RuntimeException(String.format("Benchmark %s is not properly annotated", sut));
    }
  }
}
