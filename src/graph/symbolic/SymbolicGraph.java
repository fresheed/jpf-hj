package graph.symbolic;

import graph.util.Node;
import org.jgraph.graph.AttributeMap;
import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.GraphConstants;
import org.jgrapht.experimental.dag.DirectedAcyclicGraph;
import org.jgrapht.ext.ComponentAttributeProvider;
import org.jgrapht.ext.DOTExporter;
import org.jgrapht.ext.IntegerNameProvider;
import org.jgrapht.ext.VertexNameProvider;

import java.awt.*;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

/**
 * Basically the same graph as Graph but made of SymbolicNodes. Should be merged with original one.
 */
public class SymbolicGraph extends DirectedAcyclicGraph<SymbolicNode, DefaultEdge> {
  public static final long serialVersionUID = -1;

  public SymbolicGraph() {
    super(DefaultEdge.class);
  }

  public boolean happensBefore(SymbolicNode n1, SymbolicNode n2) {
    if (n1 == n2)
      return true;
    for (DefaultEdge edge : outgoingEdgesOf(n1)) {
      SymbolicNode child = getEdgeTarget(edge);
      if (happensBefore(child, n2))
        return true;
    }
    return false;
  }

  // currently not used, but should use better way to check type
//  public boolean isIsolatedEdge(DefaultEdge e) {
//    return e.getAttributes().equals(IsolatedEdgeAttributes());
//  }

  public DefaultEdge addSpawnEdge(SymbolicNode n1, SymbolicNode n2) {
    return addEdgeExt(n1, n2, map -> {
      GraphConstants.setLineColor(map, Color.GREEN);
      GraphConstants.setLineStyle(map, 2);
      map.put("colorName", "green");
    });
  }

  public DefaultEdge addFutureEdge(SymbolicNode n1, SymbolicNode n2) {
    return addEdgeExt(n1, n2, map -> {
      GraphConstants.setLineColor(map, Color.BLUE);
      GraphConstants.setLineStyle(map, 2);
      map.put("colorName", "blue");
    });
  }

  public DefaultEdge addJoinEdge(SymbolicNode n1, SymbolicNode n2) {
    return addEdgeExt(n1, n2, map -> {
      GraphConstants.setLineColor(map, Color.RED);
      map.put("colorName", "red");
    });
  }

  public DefaultEdge addContinuationEdge(SymbolicNode n1, SymbolicNode n2) {
    return addEdgeExt(n1, n2, map -> {
      GraphConstants.setLineColor(map, Color.BLACK);
      map.put("colorName", "black");
    });
  }

  public DefaultEdge addIsolatedEdge(SymbolicNode n1, SymbolicNode n2) {
    return addEdgeExt(n1, n2, map -> {
      GraphConstants.setLineColor(map, Color.PINK);
      map.put("colorName", "pink");
    });
  }

  private DefaultEdge addEdgeExt(SymbolicNode n1, SymbolicNode n2, Consumer<AttributeMap> setup){
    DefaultEdge edge = addEdge(n1, n2);
    if (edge != null) { // in previous version it was checked for join edge
      AttributeMap attrs = new AttributeMap();
      setup.accept(attrs);
      edge.setAttributes(attrs);
    }
    return edge;
  }

  public void writeGraph(String fname) {
    IntegerNameProvider<SymbolicNode> p1 = new IntegerNameProvider<>();
    ComponentAttributeProvider<SymbolicNode> p2 = arg0 -> {
      Map<String, String> map = new HashMap<>();
      map.put("label", String.valueOf(arg0.getIndex()));
      if(arg0.error){
        map.put("color","red");
      }
      else if (arg0.isIsolated()) {
        map.put("color", "pink");
      }
      return map;
    };

    VertexNameProvider nameProvider = vertex -> {
      SymbolicNode node = (SymbolicNode)vertex;
      String name = (node.isAsync() ? "async" : (node.isJoin() ? "finish" : node.toString()));
      name = String.format("(%d): %s", ((SymbolicNode) vertex).getIndex(), name);
      return name;
    };

    ComponentAttributeProvider<DefaultEdge> p3 = edge -> {
      Map<String, String> params = new HashMap<>();
      params.put("color", (String) edge.getAttributes().get("colorName"));
      return params;
    };

    DOTExporter<SymbolicNode, DefaultEdge> exporter = new DOTExporter<SymbolicNode, DefaultEdge>(p1, nameProvider, null, p2, p3);
    try {
      exporter.export(new FileWriter(fname), this);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}

