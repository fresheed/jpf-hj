package graph.symbolic;

import fasttrack.FastTrackDetector;
import gov.nasa.jpf.Config;
import gov.nasa.jpf.JPF;
import util.Search;

/**
 * Wrapper for symbolic FastTrack algorithm allowing to restore needed information on backtrack
 */
public class SymbolicFastTrackRaceDetector extends GeneralSymbolicSearch {
  public SymbolicFastTrackRaceDetector(Config conf, JPF jpf) {
    super(conf, jpf, new FastTrackDetector());
  }
}
