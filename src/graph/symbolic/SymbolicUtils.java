package graph.symbolic;

import gov.nasa.jpf.symbc.numeric.PathCondition;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by fresheed on 14.08.19.
 */
public class SymbolicUtils {

    public static String shortPC(PathCondition pc){
        if (pc.count()==0){
            return "T";
        } else {
            List<String> lines = Arrays.asList(removePrefixes(pc.toString()).split("\n"));
            return lines.stream().skip(1).collect(Collectors.joining(" "));
        }
    }

    public static String removePrefixes(String string){
        return string.replace("_SYMINT", "")
                .replace("CONST_", "")
                .replace("(null)", "");
    }
}
