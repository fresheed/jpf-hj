package graph.symbolic;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.JPF;
import gov.nasa.jpf.symbc.numeric.PCChoiceGenerator;
import gov.nasa.jpf.vm.ChoiceGenerator;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.VM;
import util.Search;

import java.util.HashMap;
import java.util.Map;

/**
 * Keeps track of current symbolic ChoiceGenerator and passes its path condition to the detector.
 */
public class SymbolicHBSearch extends GeneralSymbolicSearch {
  private PCChoiceGenerator currentPCCG;

  public SymbolicHBSearch(Config conf, JPF jpf) {
    super(conf, jpf, new SymbolicHBDetector());
  }

  /**
   * Current path condition changes on symbolic instructions execution, so we have to check it after every instruction
   */
  @Override
  public void instructionExecuted(VM vm, ThreadInfo currentThread, Instruction nextInstruction, Instruction executedInstruction) {
    if (currentPCCG != null) {
      try {
        SymbolicHBDetector shb = (SymbolicHBDetector)detector;
        shb.setCurrentPC(currentPCCG.getCurrentPC());
      } catch (ClassCastException e){}
    }
  }

  @Override
  public void choiceGeneratorSet(VM vm, ChoiceGenerator<?> currentCG){
    try {
      currentPCCG = (PCChoiceGenerator) currentCG;
    } catch (ClassCastException e){
      // only process PCCG
    }
  }
}
