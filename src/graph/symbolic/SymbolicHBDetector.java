package graph.symbolic;

import gov.nasa.jpf.symbc.numeric.LogicalORLinearIntegerConstraints;
import gov.nasa.jpf.symbc.numeric.PathCondition;
import org.jgraph.graph.DefaultEdge;
import org.jgrapht.Graphs;
import util.AddableChoiceGenerator;
import util.Detector;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Keeps track of current path condition and passes it to symbolic graph. <br/>
 */
public class SymbolicHBDetector extends Detector {
  protected SymbolicGraph graph = new SymbolicGraph();
  protected Map<Integer, SymbolicNode> currentNodes = new HashMap<>();
  protected SymbolicNode isolatedNode = null;
  protected DefaultEdge isolatedEdge = null;
  protected Set<Integer> joinedFutures = new HashSet<>();
  protected int tasks = 0;
  private int count = 0;
  private boolean race = false;

  private List<PathCondition> pcHistory = new ArrayList<>();

  // These caches allow to decide whether a new node should be created or an existing one should be returned
  private Map<IntTriple, SymbolicNode>
          forkContinuations =new HashMap<>(),
          forkChilds=new HashMap<>(),
          joinNodes=new HashMap<>();

  public SymbolicHBDetector() {
    SymbolicNode root = SymbolicNode.mkActivityNode();
    graph.addVertex(root);
    currentNodes.put(0, root);
    pcHistory.add(new PathCondition());
  }

  public void setCurrentPC(PathCondition pc){
    PathCondition currentPC = pcHistory.get(pcHistory.size()-1);
    if (!pc.equals(currentPC)){
      debug("Setting PC = ... "+SymbolicUtils.shortPC(pc)+" of class"+pc.header.getClass());
      pcHistory.add(pc);
    }
  }

  @Override
  public boolean isDependent(AddableChoiceGenerator cg1, AddableChoiceGenerator cg2) {
//    if (cg1 == cg2) return false;
//    if (!cg1.isIsolateCG()) return false; //if not created for isolation don't reorder
//    if (!cg2.isIsolateCG()) return false;
//    if (cg1.getObject() == null || cg2.getObject() == null) {
//        throw new RuntimeException("Stored Nodes in CG's missing");
//    }
//    SymbolicNode isoNode1 = (SymbolicNode) cg1.getObject();
//    SymbolicNode isoNode2 = (SymbolicNode) cg2.getObject();
//    if (isoNode1.intersection(isoNode2).isEmpty()) {
//        return false;
//    }
//    return true;

    // should we check for satisfiability here?
    throw new RuntimeException("Not implemented yet");
  }

  @Override
  public void resetState(Object state) {
    CompGraphToolState toolState = new CompGraphToolState((CompGraphToolState)state);
    //graph = toolState.graph;
    isolatedNode = toolState.isolatedNode;
    currentNodes = toolState.currentNodes;
    joinedFutures = toolState.joinedFutures;
    tasks = toolState.tasks;
  }

  @Override
  public Object getImmutableState() {
    return new CompGraphToolState(graph, isolatedNode, currentNodes, joinedFutures, tasks);
  }

  @Override
  public void handleRead(int tid, String uniqueLabel) {
    saveAccess(tid, uniqueLabel, false);
  }

  @Override
  public void handleWrite(int tid, String uniqueLabel) {
    saveAccess(tid, uniqueLabel, true);
  }

  private void saveAccess(int tid, String label, boolean isWrite){
    debug(String.format("(%s, TID_N %d_%d): '%s')", isWrite?"Write":"Read", tid,
            currentNodes.get(tid).getIndex(), label.substring(label.lastIndexOf("."))));
    currentNodes.get(tid).addAccess(label, isWrite, getCurrentPC());
  }

  private PathCondition getCurrentPC(){
    return pcHistory.get(pcHistory.size()-1);
  }

  private void debug(String msg){
    System.out.println(msg);
  }

  @Override
  public void handleAcquire(int tid) {
//    SymbolicNode isoNode = SymbolicNode.mkIsolatedNode();
//    graph.addVertex(isoNode);
//    graph.addContinuationEdge(currentNodes.get(tid), isoNode);
//    currentNodes.put(tid, isoNode);
//    if (isolatedNode != null) {
//      isolatedEdge = graph.addIsolatedEdge(isolatedNode, isoNode);
//    }
//    isolatedNode = isoNode;
    throw new RuntimeException("Not implemented yet");
  }

  @Override
  public void handleRelease(int tid, AddableChoiceGenerator cg) {
//    if (isolatedEdge != null && graph != null && graph.containsEdge(isolatedEdge)) { //if we created an isolation edge
//        //check to see if it was necessary
//        SymbolicNode sourceNode = graph.getEdgeSource(isolatedEdge);
//        SymbolicNode targetNode = graph.getEdgeTarget(isolatedEdge);
//        if (sourceNode.intersection(targetNode).size() == 0) {
//
//     System.out.println("deleting unnecessary edge");
//            graph.removeEdge(isolatedEdge);
//            isolatedEdge = null;
//        }
//    }
//    if (isolatedNode == null) {
//        throw new RuntimeException("Exited isolated section without creating isolated node");
//    }
//    cg.putObject(isolatedNode);
//    SymbolicNode continueNode = SymbolicNode.mkActivityNode();
//    graph.addVertex(continueNode);
//    graph.addContinuationEdge(currentNodes.get(tid), continueNode);
//    currentNodes.put(tid, continueNode);
    throw new RuntimeException("Not implemented yet");
  }

  @Override
  public void handleFork(int parent, int child, boolean future) {
    if (currentNodes.get(parent).getIndex() != 0) {
      IntTriple params = new IntTriple(parent, child, currentNodes.get(parent).getIndex());
      if (!forkContinuations.containsKey(params)){
        SymbolicNode forkNode = SymbolicNode.mkForkNode();
        SymbolicNode continueNode = SymbolicNode.mkActivityNode();
        SymbolicNode childNode = SymbolicNode.mkActivityNode();
        graph.addVertex(forkNode);
        graph.addVertex(continueNode);
        graph.addVertex(childNode);
        graph.addContinuationEdge(currentNodes.get(parent), forkNode);
        if (future)
          graph.addFutureEdge(forkNode, childNode);
        else
          graph.addSpawnEdge(forkNode, childNode);
        graph.addContinuationEdge(forkNode, continueNode);
        forkContinuations.put(params, continueNode);
        forkChilds.put(params, childNode);
      }
      currentNodes.put(parent, forkContinuations.get(params));
      currentNodes.put(child, forkChilds.get(params));
    } else {
      addInitNode(parent, child);
    }
    tasks++;
  }

  private void addInitNode(int parent, int child) {
    // Special case. We are only concerned with the code inside launchHabaneroApp
    SymbolicNode continueNode = SymbolicNode.mkActivityNode();
    graph.addVertex(continueNode);
    graph.addContinuationEdge(currentNodes.get(parent), continueNode);
    currentNodes.put(parent, continueNode);
    currentNodes.put(child, continueNode);
  }

  @Override
  public void handleJoin(int parent, int child, boolean finish) {
    if (!currentNodes.get(parent).isJoin()) {
      IntTriple params=new IntTriple(parent, child, currentNodes.get(parent).getIndex());
      if (!joinNodes.containsKey(params)) {
        SymbolicNode joinNode = SymbolicNode.mkJoinNode();
        graph.addVertex(joinNode);
        graph.addJoinEdge(currentNodes.get(parent), joinNode);
        joinNodes.put(params, joinNode);
      }
      currentNodes.put(parent, joinNodes.get(params));
    }
    if (!finish) {
      joinedFutures.add(child);
    }
    if (!finish || !joinedFutures.contains(child)) {
      // may be repeated, but should not fail
      graph.addJoinEdge(currentNodes.get(child), currentNodes.get(parent));
    }
  }

  public boolean checkGraph(SymbolicGraph graph) {
    List<SymbolicNode> sortedNodes = new ArrayList<>();
    graph.iterator().forEachRemaining(it -> sortedNodes.add(it));
    for (ListIterator<SymbolicNode> it1 = sortedNodes.listIterator(); it1.hasNext(); ){
      SymbolicNode node1 = it1.next();
      for (Iterator<SymbolicNode> it2 = sortedNodes.listIterator(it1.nextIndex()); it2.hasNext(); ){
        SymbolicNode node2 = it2.next();
        if (node1.isReadWrite() && node2.isReadWrite()
                && !(node1.isIsolated() && node2.isIsolated())
                && !graph.happensBefore(node1, node2)
                && node1.mayConflict(node2)){
          System.out.println("Arrays are not checked!");
          System.out.println(node1.getIndex() + " races with " + node2.getIndex());
          node1.error = true;
          node2.error = true;
          return true;
        }
      }
    }
    System.out.println("Arrays are not checked!");
    return false;
  }

  @Override
  public void handleHalt(String programName) {
    if(debug) {
      System.out.println("Writing graph " + (++count));
      System.out.println("Used PCs: "+pcHistory.stream().map(SymbolicUtils::shortPC).collect(Collectors.toList()));
      graph.writeGraph("./dots/" + programName + "-" + count + ".dot");
    }
    race |= checkGraph(graph);
  }

  public SymbolicGraph getGraph() {
    return graph;
  }

  public boolean hasRace() {
    return race;
  }

  public String error() {
    return "Data Race Detected";
  }

  class CompGraphToolState {
    final SymbolicGraph graph;
    final SymbolicNode isolatedNode;
    final Map<Integer, SymbolicNode> currentNodes;
    final Set<Integer> joinedFutures;
    final int tasks;

    CompGraphToolState(SymbolicGraph graph, SymbolicNode isolatedNode, Map<Integer, SymbolicNode> currentNodes, Set<Integer> joinedFutures, int tasks) {
      this.graph = new SymbolicGraph();
      Graphs.addGraph(this.graph, graph);
      this.isolatedNode = isolatedNode;
      this.currentNodes = new HashMap<>(currentNodes);
      this.joinedFutures = new HashSet<>(joinedFutures);
      this.tasks = tasks;
    }

    CompGraphToolState(CompGraphToolState state) {
      this(state.graph, state.isolatedNode, state.currentNodes, state.joinedFutures, state.tasks);
    }

    @Override
    public String toString() {
      return "";
    }
  }
}

class IntTriple {
  final int x, y, z;
  IntTriple(int x, int y, int z){
    this.x=x;
    this.y=y;
    this.z=z;
  }

  @Override
  public boolean equals(Object other){
    try {
      IntTriple otherTriple=(IntTriple)other;
      return (this.x==otherTriple.x && this.y==otherTriple.y && this.z==otherTriple.z);
    } catch (ClassCastException e){
      return false;
    }
  }

  @Override
  public int hashCode() {
    return x ^ y ^ z;
  }
}