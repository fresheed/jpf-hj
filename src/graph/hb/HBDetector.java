package graph.hb;

import util.Detector;
import graph.util.Graph;
import graph.util.Node;
import org.jgraph.graph.DefaultEdge;
import org.jgrapht.Graphs;
import util.AddableChoiceGenerator;

import java.util.*;

public class HBDetector extends Detector {
  protected Graph graph = new Graph();
  protected Map<Integer, Node> currentNodes = new HashMap<>();
  protected Node isolatedNode = null;
  protected DefaultEdge isolatedEdge = null;
  protected Set<Integer> joinedFutures = new HashSet<>();
  protected int tasks = 0;
  private int count = 0;
  private boolean race = false;

  public HBDetector() {
    Node root = Node.mkActivityNode();
    graph.addVertex(root);
    currentNodes.put(0, root);
  }

  @Override
  public boolean isDependent(AddableChoiceGenerator cg1, AddableChoiceGenerator cg2) {
    if (cg1 == cg2) return false;
    if (!cg1.isIsolateCG()) return false; //if not created for isolation don't reorder
    if (!cg2.isIsolateCG()) return false;
    if (cg1.getObject() == null || cg2.getObject() == null) {
        throw new RuntimeException("Stored Nodes in CG's missing");
    }
    Node isoNode1 = (Node) cg1.getObject();
    Node isoNode2 = (Node) cg2.getObject();
    if (isoNode1.intersection(isoNode2).isEmpty()) {
        return false;
    }
    return true;
  }

  @Override
  public void resetState(Object state) {
    CompGraphToolState toolState = new CompGraphToolState((CompGraphToolState)state);
    graph = toolState.graph;
    isolatedNode = toolState.isolatedNode;
    currentNodes = toolState.currentNodes;
    joinedFutures = toolState.joinedFutures;
    tasks = toolState.tasks;
  }

  @Override
  public Object getImmutableState() {
    return new CompGraphToolState(graph, isolatedNode, currentNodes, joinedFutures, tasks);
  }

  @Override
  public void handleRead(int tid, String uniqueLabel) {
    currentNodes.get(tid).addAccess(uniqueLabel, false);
  }

  @Override
  public void handleWrite(int tid, String uniqueLabel) {
    currentNodes.get(tid).addAccess(uniqueLabel, true);
  }

  @Override
  public void handleAcquire(int tid) {
    Node isoNode = Node.mkIsolatedNode();
    graph.addVertex(isoNode);
    graph.addContinuationEdge(currentNodes.get(tid), isoNode);
    currentNodes.put(tid, isoNode);
    if (isolatedNode != null) {
      isolatedEdge = graph.addIsolatedEdge(isolatedNode, isoNode);
    }
    isolatedNode = isoNode;
  }

  public Node getIsolatedNode() {
    return isolatedNode;
  }

  @Override
  public void handleRelease(int tid, AddableChoiceGenerator cg) {
    if (isolatedEdge != null && graph != null && graph.containsEdge(isolatedEdge)) { //if we created an isolation edge
        //check to see if it was necessary
        Node sourceNode = graph.getEdgeSource(isolatedEdge);
        Node targetNode = graph.getEdgeTarget(isolatedEdge);
        if (sourceNode.intersection(targetNode).size() == 0) {
     
     System.out.println("deleting unnecessary edge");
            graph.removeEdge(isolatedEdge);
            isolatedEdge = null; 
        }
    }
    if (isolatedNode == null) {
        throw new RuntimeException("Exited isolated section without creating isolated node");
    }
    cg.putObject(isolatedNode);
    Node continueNode = Node.mkActivityNode();
    graph.addVertex(continueNode);
    graph.addContinuationEdge(currentNodes.get(tid), continueNode);
    currentNodes.put(tid, continueNode);
  }

  @Override
  public void handleFork(int parent, int child, boolean future) {
    if (currentNodes.get(parent).getIndex() != 0) {
      Node forkNode = Node.mkForkNode();
      Node continueNode = Node.mkActivityNode();
      Node childNode = Node.mkActivityNode();
      graph.addVertex(forkNode);
      graph.addVertex(continueNode);
      graph.addVertex(childNode);
      graph.addContinuationEdge(currentNodes.get(parent), forkNode);
      if (future)
        graph.addFutureEdge(forkNode, childNode);
      else
        graph.addSpawnEdge(forkNode, childNode);
      graph.addContinuationEdge(forkNode, continueNode);
      currentNodes.put(parent, continueNode);
      currentNodes.put(child, childNode);
    } else {
      // Special case. We are only concerned with the code inside launchHabaneroApp
      Node continueNode = Node.mkActivityNode();
      graph.addVertex(continueNode);
      graph.addContinuationEdge(currentNodes.get(parent), continueNode);
      currentNodes.put(parent, continueNode);
      currentNodes.put(child, continueNode);
    }
    tasks++;
  }

  @Override
  public void handleJoin(int parent, int child, boolean finish) {
    if (!currentNodes.get(parent).isJoin()) {
      Node joinNode = Node.mkJoinNode();
      graph.addVertex(joinNode);
      graph.addJoinEdge(currentNodes.get(parent), joinNode);
      currentNodes.put(parent, joinNode);
    }
    if (!finish) {
      joinedFutures.add(child);
    }
    if (!finish || !joinedFutures.contains(child))
      graph.addJoinEdge(currentNodes.get(child), currentNodes.get(parent));
  }

  public boolean checkGraph(Graph graph) {
    List<Node> nodes = new ArrayList<>();
    Iterator<Node> it = graph.iterator();
    while (it.hasNext())
      nodes.add(it.next());
    for (int i = 0; i < nodes.size(); i++)
      for (int j = i + 1; j < nodes.size(); j++) {
        if (nodes.get(i).isReadWrite()
                && nodes.get(j).isReadWrite()
                && !(nodes.get(j).isIsolated() && nodes.get(i).isIsolated())
                && !graph.happensBefore(nodes.get(i), nodes.get(j))
                && nodes.get(i).intersection(nodes.get(j)).size() > 0
                ) {
          System.out.println(nodes.get(i) + " races with " + nodes.get(j));
          nodes.get(i).error = true;
          nodes.get(j).error = true;
          return true;
        }
      }
    return false;
  }

  @Override
  public void handleHalt(String programName) {
    if(debug) {
      System.out.println("Writing graph " + (++count));
      graph.writeGraph("./dots/" + programName + "-" + count + ".dot");
    }
    race |= checkGraph(graph);
  }

  public void writeGraph(String fname) {
    graph.writeGraph(fname);
  }

  public Graph getGraph() {
    return graph;
  }

  public int getTaskCount() {
    return tasks;
  }

  public boolean hasRace() {
    return race;
  }

  public String error() {
    return "Data Race Detected";
  }

  class CompGraphToolState {
    final Graph graph;
    final Node isolatedNode;
    final Map<Integer, Node> currentNodes;
    final Set<Integer> joinedFutures;
    final int tasks;

    CompGraphToolState(Graph graph, Node isolatedNode, Map<Integer, Node> currentNodes, Set<Integer> joinedFutures, int tasks) {
      this.graph = new Graph();
      Graphs.addGraph(this.graph, graph);
      this.isolatedNode = isolatedNode;
      this.currentNodes = new HashMap<>(currentNodes);
      this.joinedFutures = new HashSet<>(joinedFutures);
      this.tasks = tasks;
    }

    CompGraphToolState(CompGraphToolState state) {
      this(state.graph, state.isolatedNode, state.currentNodes, state.joinedFutures, state.tasks);
    }

    @Override
    public String toString() {
      return "";
    }
  }
}
