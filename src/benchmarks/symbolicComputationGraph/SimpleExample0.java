package benchmarks.symbolicComputationGraph;

import edu.rice.hj.api.HjRunnable;
import edu.rice.hj.api.SuspendableException;
import gov.nasa.jpf.vm.Verify;
import hj.runtime.wsh.Activity;
import hj.runtime.wsh.SuspendableActivity;
import hj.util.SyncLock;

import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static edu.rice.hj.Module0.finish;
import static edu.rice.hj.Module0.launchHabaneroApp;
import static edu.rice.hj.Module1.async;

/**
 * Created by fresheed on 18.06.19.
 */
public class SimpleExample0 {
    static int a = 0, b = 0;
    // cr bp method_invoke=benchmarks.symbolicComputationGraph.SimpleExample0:run

    public int run0(int i){
        //int u = Verify.getInt(0, 1);
        int u = i;
        if (u==0){
            return 11;
        } else {
            return 22;
        }
    }

    public int run1(int i){
        Activity main = new SuspendableActivity(() -> {}, true);
        Activity add = new SuspendableActivity(() -> {}, true);
        main.start();

        main.startFinish();
        add.start();
        main.stopFinish();

        return 55;
    }

}

