package benchmarks.symbolicComputationGraph;

import edu.rice.hj.api.HjRunnable;
import edu.rice.hj.api.HjSuspendable;
import edu.rice.hj.api.SuspendableException;

import static edu.rice.hj.Module0.finish;
import static edu.rice.hj.Module0.launchHabaneroApp;
import static edu.rice.hj.Module1.async;
import static edu.rice.hj.Module2.isolated;

/**
 * Created by fresheed on 19.07.19.
 */
public class INonracy {
    static int a = 0;

    public static void main(String[] args) {
        new INonracy().run(0);
    }

    public int run(int i) {
        launchHabaneroApp(new HjSuspendable() {
            @Override
            public void run() throws SuspendableException {
                finish(new HjSuspendable() {
                    @Override
                    public void run()  throws SuspendableException {
                        async(new HjRunnable() {
                            @Override
                            public void run(){
                                isolated(new HjRunnable() {
                                    @Override
                                    public void run() {
                                        a = 1;
                                    }
                                });
                            }
                        });
                        async(new HjRunnable() {
                            @Override
                            public void run(){
                                isolated(new HjRunnable() {
                                    @Override
                                    public void run() {
                                        a = 2;
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
        System.out.println("Habanero code executed");
        return 99;
    }


}
