package benchmarks.symbolicComputationGraph;

import edu.rice.hj.api.HjRunnable;
import edu.rice.hj.api.HjSuspendable;
import edu.rice.hj.api.SuspendableException;

import static edu.rice.hj.Module0.finish;
import static edu.rice.hj.Module1.async;
import static edu.rice.hj.Module1.launchHabaneroApp;
import static edu.rice.hj.Module2.isolated;

public class SimpleExample3 {

    static int a = 0, b = 0;

    public static void main(String[] args) {
        new SimpleExample3().run(0);
    }

    public int run(int i){
        launchHabaneroApp(() -> {
            async(() -> {a = 33;});
            if (i > 10) {
                a = 11;
            } else {
                b = 22;
            }
        });
        return a;
    }
}

