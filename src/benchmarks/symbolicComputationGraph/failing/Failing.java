package benchmarks.symbolicComputationGraph.failing;

import static edu.rice.hj.Module0.finish;
import static edu.rice.hj.Module0.launchHabaneroApp;

/**
 * Created by fresheed on 19.07.19.
 */
public class Failing {
    static int a = 0;

    public static void main(String[] args) {
        new Failing().run(0);
    }

    public int run(int i) {
        if (i != i){
            System.out.println("Unsatisfiable branch");
        }
        System.out.println("Habanero code executed");
        return 99;
    }
}
