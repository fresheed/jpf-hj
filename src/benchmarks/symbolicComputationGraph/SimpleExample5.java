package benchmarks.symbolicComputationGraph;

import edu.rice.hj.api.HjRunnable;
import edu.rice.hj.api.HjSuspendable;
import edu.rice.hj.api.SuspendableException;

import static edu.rice.hj.Module0.finish;
import static edu.rice.hj.Module0.waitAny;
import static edu.rice.hj.Module1.async;
import static edu.rice.hj.Module1.launchHabaneroApp;

public class SimpleExample5 {

    static int a = 10, b = 0;

    public static void main(String[] args) {
        launchHabaneroApp(() ->
                finish(() -> {
                    a = a % 2;
                    finish(() -> async(() -> a *= 10));

                    if (a > 5){
                        async(() -> b = 1);
                    }
                    async(() -> b = 2);
                        }
                )
        );
    }
}

