package benchmarks.symbolicComputationGraph.staticGraph;

import edu.rice.hj.api.HjRunnable;
import edu.rice.hj.api.HjSuspendable;
import edu.rice.hj.api.SuspendableException;

import static edu.rice.hj.Module0.launchHabaneroApp;
import static edu.rice.hj.Module1.async;

/**
 * Created by fresheed on 19.07.19.
 */
@RaceDetectionResult(hasRace = false)
public class AFNonracy {
    static int a = 0;
    static int b = 0;
    static int c = 0;
    public static void main(String[] args) {
        new AFNonracy().run(0);
    }

    public int run(int i) {
        launchHabaneroApp(new HjSuspendable() {
            @Override
            public void run() throws SuspendableException {
                async(new HjRunnable() {
                    @Override
                    public void run() {
                        if (i < 0) {
                            a = c;
                        }
                    }
                });
                if (i > 0) {
                    c = 2;
                }
            }
        });
        return 99;
    }


}
