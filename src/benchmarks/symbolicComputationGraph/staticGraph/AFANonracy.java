package benchmarks.symbolicComputationGraph.staticGraph;

import edu.rice.hj.api.HjRunnable;
import edu.rice.hj.api.HjSuspendable;
import edu.rice.hj.api.SuspendableException;

import static edu.rice.hj.Module0.finish;
import static edu.rice.hj.Module0.launchHabaneroApp;
import static edu.rice.hj.Module1.async;

/**
 * Created by fresheed on 19.07.19.
 */
@RaceDetectionResult(hasRace = false)
public class AFANonracy {
    static int a = 0;

    public static void main(String[] args) {
        new AFANonracy().run(0);
    }

    public int run(int i) {
        launchHabaneroApp(new HjSuspendable() {
            @Override
            public void run() throws SuspendableException {
                finish(new HjSuspendable() {
                    @Override
                    public void run() throws SuspendableException {
                        async(new HjRunnable() {
                            @Override
                            public void run() {
                                a = 2;
                            }
                        });
                    }
                });
                async(new HjRunnable() {
                    @Override
                    public void run() {
                        a = 1;
                    }
                });
            }
        });
        System.out.println("Habanero code executed");
        return 99;
    }


}
