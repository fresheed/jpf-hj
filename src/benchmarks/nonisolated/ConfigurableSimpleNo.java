package benchmarks.nonisolated;

import benchmarks.misc.configurable.Block;

public class ConfigurableSimpleNo {
  public static void main(String[] args) {
    Block thread = Block.create().write();
    Block.create().write(thread.get()).async(thread).emmit("ConfiguredSimpleNo");
  }
}
