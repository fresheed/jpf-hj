package benchmarks.misc.configurable;

class Synchronization extends Block {
  public boolean async = false;

  private Synchronization() { }

  public Synchronization(Block block) {
    this.add(block);
  }

  public Synchronization(Block block, boolean async) {
    this.add(block);
    this.async = async;
  }

  public String toString() {
    if(async) return "Async"+ super.toString();
    return "Finish"+super.toString();
  }
}
