package benchmarks;

import static edu.rice.hj.Module0.finish;
import static edu.rice.hj.Module0.launchHabaneroApp;
import static edu.rice.hj.Module1.async;
import static edu.rice.hj.Module2.isolated;

import edu.rice.hj.api.HjRunnable;
import edu.rice.hj.api.HjSuspendable;


public class DataRaceIsolateLoop {

    public static final int NUMLOOPS = 3;
    private static int g = 0;
    private static int loopNum;

    public static void main(final String[] args) {
        launchHabaneroApp(new HjSuspendable() {
        	public void run() {
                finish(new HjSuspendable() {
                	public void run() {
                        for (int i = 1; i < NUMLOOPS; i++) {
                            p();
                            //System.out.println("Thread " + i + " before");
                            async(new HjRunnable() {
                                    public void run() {
                                        p();
                                    }
                            });	
                            //System.out.println("Thread " + i + " after");
                        }
                        //acquireW(DataRaceIsolateLoop.g);
                        //DataRaceIsolateLoop.g = 0;
                        //releaseW(DataRaceIsolateLoop.g);
                        //System.out.println("Thread " + 0 + " before");
                        p();
                        //System.out.println("Thread " + 0 + " after");
                	}
                });
                
        	}
        });
    }

    private static void p() {
    	isolated(new HjRunnable() {
    		public void run() {
    			System.out.println("Isolated: " + loopNum++);
    	        DataRaceIsolateLoop.g = 2;
    		}	
    	});	
//        acquireW(DataRaceIsolateLoop.g);
//        DataRaceIsolateLoop.g = 2;
//        releaseW(DataRaceIsolateLoop.g);
    }
  
}
