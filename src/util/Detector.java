package util;

import gov.nasa.jpf.vm.ChoiceGenerator;
import gov.nasa.jpf.vm.ThreadInfo;
import graph.util.Node;

public abstract class Detector {
  public int haltCount = 0;
  public Search owner;
  private boolean DPORWarningPrinted = false;
  // For rewinding
  public abstract void resetState(Object state);
  public abstract Object getImmutableState();
  // Event handling
  public void handleRead(int tid, String uniqueLabel) {}
  public void handleWrite(int tid, String uniqueLabel) {}
  public void handleAcquire(int tid) {}
  public void handleRelease(int tid, AddableChoiceGenerator cg) {}
  public void handleFork(int parent, int child, boolean future) {}
  public void handleJoin(int parent, int child, boolean finish) {}
  public void handleHalt(String programName) {
      //System.out.println("HALT COUNT: " + ++haltCount);
  }
  public Node getIsolatedNode() {return null;}
  public boolean useDC = true;
  public boolean debug = true;
  public static final String errorMessage = "Data Race Detected";
  // Properties
  public boolean hasRace() {
    return false;
  }


  public Detector() {

  }

  public Detector(boolean useDC) {
    this.useDC = useDC;
  }

  public String error() {
    return errorMessage;
  }

  public boolean isDependent(AddableChoiceGenerator previousCG, AddableChoiceGenerator cg) {
    if(!DPORWarningPrinted) {
      System.err.println("DPOR not implemented in detector.");
      DPORWarningPrinted = true;
    }
    return true;
  };

  public void setUseDC(boolean useDC) {
    this.useDC = useDC;
  }
}
