package util;

import util.Search;
import graph.util.Node;
import gov.nasa.jpf.vm.ChoiceGenerator;
import gov.nasa.jpf.vm.choice.ThreadChoiceFromSet;
import gov.nasa.jpf.vm.ThreadInfo;

import java.util.*;

import util.Search.RaceDetectorState;

public class AddableChoiceGenerator extends ThreadChoiceFromSet {

    private boolean isIsolateCG = false;
    private ArrayList<ThreadInfo> runnables = null;
    private Search owner = null;
    private RaceDetectorState state;
    private Object storedObject;
    private int valuesSize;

	public AddableChoiceGenerator(String id, ThreadInfo[] runnables, Search owner, RaceDetectorState state, ThreadInfo tiCurrent) {
		super(id, new ThreadInfo[10], true);
        this.values[0] = tiCurrent;
        this.runnables =  new ArrayList(Arrays.asList(runnables));
        valuesSize = 0;
        this.owner = owner;
        this.state = state;
        this.isIsolateCG = true;
	}

	public AddableChoiceGenerator(String id, ThreadInfo[] runnables, boolean isSchedulePoint) {
		super(id, runnables, isSchedulePoint);
        this.isIsolateCG = false;
	}

    public void addAllRunnables() {
            List<ThreadInfo> choiceList = Arrays.asList(values);
        for(ThreadInfo choice : this.runnables) {
            if (!choiceList.contains(choice)) {
                this.add(choice);
            }
        }
    }

    private void add(ThreadInfo choice) {
        valuesSize++;
        if (valuesSize >= values.length) {
           values = Arrays.copyOf(values, values.length * 2);
        }
        values[valuesSize] = choice;
    }

    public void addNextChoice(ThreadInfo next) {
         if (Arrays.asList(values).contains(next)) return;
         if (runnables.contains(next)) add(next);
         else {
            //if we can't run the thread we need to run all the other threads
            //so that that thread we need to run will eventually be run (or is unreachable)
            for (ThreadInfo choice : this.runnables) { 
                if (!Arrays.asList(values).contains(choice)) {
                    add(choice);
                }
            }
            //System.out.println("Requested to run thread that is unrunnable");
         }
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("CG " + id + ":\t[");
        for (ThreadInfo choice : Arrays.asList(values)) {
            if (values[count] == choice) sb.append("*");
            sb.append(choice.getId());
            sb.append(", ");
        }
        if (Arrays.asList(values).isEmpty()) sb.append("\b\b]");
        return (sb.toString());
    }

    public boolean isIsolateCG() {
        return this.isIsolateCG;
    }
    public void putObject(Object o) {
        this.storedObject = o;
    }

    public Object getObject() {
        return this.storedObject;
    }

	@Override
    public void advance() {
        //if(id == "ISOLATED") System.out.println("CG " + id + " advanced");
        if (state != null) owner.resetState(state);
        super.advance();
    }

}
