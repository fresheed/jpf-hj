package util;

import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.jvm.bytecode.InstructionFactory;
import gov.nasa.jpf.vm.ChoiceGenerator;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.VM;

import java.util.Arrays;

/**
 * Created by fresheed on 19.07.19.
 */
public class DebugListener extends ListenerAdapter {

    @Override
    public void instructionExecuted (VM vm, ThreadInfo ti, Instruction nextInsn, Instruction executedInsn) {
        if (executedInsn.getMnemonic().equals("ifeq")){
            System.out.println("---------- ifeq executed, "+executedInsn.getClass());
        }
    }

  @Override
  public void choiceGeneratorSet(VM vm, ChoiceGenerator cg){
    writeLog("### set CG: "+describeCG(cg));
    for (ThreadInfo ti: vm.getThreadList()){
      writeLog(ti.getStateDescription());
      writeLog(ti.getStackTrace());
    }
  }

  @Override
  public void choiceGeneratorAdvanced (VM vm, ChoiceGenerator cg){
    writeLog("### CG advanced: "+describeCG(cg)+", picked choice: "+cg.getNextChoice());
  }

  @Override
  public void choiceGeneratorProcessed (VM vm, ChoiceGenerator cg){
    writeLog("### processed CG "+describeCG(cg));
  }

  private void writeLog(String msg){
      System.out.println("------ "+msg);
  }

  private String describeCG(ChoiceGenerator cg){
    return String.format("%s: %d/%d choice of %s (%s)", cg.getClass().getSimpleName(),
            cg.getProcessedNumberOfChoices(), cg.getTotalNumberOfChoices(),
            cg.getChoiceType().getSimpleName(), Arrays.toString(cg.getAllChoices()));
  }

}
