benchmark_prefix='benchmarks.symbolicComputationGraph.staticGraph'
benchmark=$benchmark_prefix.$2

config=$1
case $config in
  symbolicGraph|symbolicFasttrack) echo "Using config: $config";;
  *) echo "Unknown config: $config"; exit 1 ;;
esac


./gradlew run -Pconfig=$config -Pbenchmark=$benchmark


if [ $config == "symbolicGraph" ]; then
  # render only most recent graph
  dots_count=$(ls -1q dots/$benchmark* | wc -l)
  dots=(ls dots/$benchmark*)
  dot -Tpng ${dots[dots_count]} > dots/out.png
fi
