% Intended LaTeX compiler: xelatex
\documentclass[11pt, sigconf]{acmart}
\usepackage{tikz}
\usetikzlibrary{shapes,positioning,arrows,decorations,calc}
\tikzset{
  -latex,auto,node distance =0.3 cm and 0.3 cm,semithick,
  bd/.style = {->,> = latex', red, thick, },
  cond/.style = {->,> = latex', blue, thick, },
  node/.style ={draw, minimum width = 0.1 cm},
  comment/.style ={text=gray},
}
\newcommand{\po}[3]{\path (#1) edge node {#3} (#2)}
\newcommand{\sync}[3]{\path[bd, #3] (#1) edge node {} (#2)}
\newcommand{\cond}[3]{\path[cond] (#1) edge node {#3} (#2)}

\acmConference[JPF'19]{Java Pathfinder Workshop}{Nov 2019}{San Diego, California, United States} 
\acmYear{2019}
\copyrightyear{2019}
\acmDOI{}

\title{Symbolic data race detection for Habanero programs}

\author{Egor Namakonov}
\affiliation{%
  \institution{Saint Petersburg State University}
  \city{Saint Petersburg}
  \state{Russia} 
}
\email{e.namakonov@gmail.com}

\author{Eric Mercer}
\affiliation{%
  \institution{Brigham Young University}
  \city{Provo} 
  \state{Utah} 
}
\email{egm@cs.byu.edu}

\author{Pavel Parizek}
\affiliation{%
  \institution{Charles University}
  \city{Prague} 
  \state{Czechia} 
}
\email{parizek@d3s.mff.cuni.cz}

\author{Kyle Storey}
\affiliation{%
  \institution{Brigham Young University}
  \streetaddress{Address}
  \city{Provo} 
  \state{Utah} 
  \postcode{Zipcode}
}
\email{kyle.r.storey@gmail.com}

\renewcommand{\shortauthors}{E. Namakonov et al.}

\newcommand{\todo}[1]{{\Large \color{red} #1}}
\newcommand{\term}[1]{\textit{#1}}

\ccsdesc{Model checking}
\keywords{Data race detection, Symbolic execution, Habanero, Java Pathfinder, Symbolic Pathfinder}


\begin{document}
\maketitle

\section*{Problem statement}

A data race occurs when two conflicting memory accesses occur without any synchronization. Under various memory models (like C++ and Java), the presence of data races brings some form of undefined behavior into program execution. Hence, data race detection is an important problem in the area of program verification.

Model checking is a common method of validating program properties, including absence of data races. Basically, it just enumerates and checks different states of program execution. The practical use of this method is limited, though, because of the exhaustive nature of algorithm causing so-called state explosion. 

Some parallelism models provide properties that ease reasoning about data races. In this paper we consider the Habanero model, especially its Java implementation. There is a graph-based algorithm\cite{original-algorithm} which greatly simplifies data race detection in Habanero programs: for every total order on mutually exclusive blocks, only one program run is required to establish data race freedom on a given input. 

However, the input value causing a race should be somehow determined. Besides, the state explosion problem remains since the algorithm has to check all possible schedules the number of which is factorial of the amount of the mutually exclusive blocks. 

\section*{Proposed solution}

We address these problems by employing symbolic execution into the original graph-based algorithm. Informally, instead of enumerating all schedules and checking whether any of them has a race, we find conflicting instructions and then check conditions under which the race can happen. 

The execution of a task parallel program can be represented as a DAG. Its vertices are either regular nodes or synchronization nodes. Each regular node holds read and write sets of variables that are accessed between two synchronization events. Graph's edges represent happens-before relation in the program.

The original algorithm observes the execution of the program on a fixed input to build the graph. Then, the graph is topologically sorted and all pairs of unordered nodes are checked for the conflict according to their read and write sets. This process is repeated for every possible order of the mutually exclusive blocks. Since such graphs represent the execution of a program on a specific input value, they may not contain some program instructions due to control flow branching. 

Our project \cite{repo} extends this algorithm for the case of symbolic execution. The execution graph becomes symbolic: read and write sets account for accesses in all control flow branches between two synchronization events. To distinguish between different execution branches, these sets now hold not just variable names, but also path conditions under which the specific variable is being read or written correspondingly. This approach reminds a value summaries technique \cite{value-summaries} which significantly speeds up symbolic execution. To check if there is a race on variable included in access sets of two such nodes, we must check not only their unorderedness but also the satisfiability of conjunction of corresponding path conditions. 

\begin{figure}
  \begin{minipage}{0.3\linewidth}
\begin{verbatim}
finish(() -> 
  async(() -> 
    a = 1));
if (b > 0) {
  async(() ->
    print(a));
}
if (b < 0){
  a = 2;
} else {
  a = a + 1;
}
\end{verbatim}
  \end{minipage} \hfill
  \begin{minipage}{0.6\linewidth}
\begin{tikzpicture}
  \node[node] (s0) at (0, -0) {$async$};
  \node[node] (s1) at (2, -1) {$R_a: \bot,\ W_a: \top$}; 
  \node[node] (s2) at (0, -2) {$finish$};
  \node[node] (s4) at (0, -3) {$async$};
  \node[node] (s5) at (2, -4) {$R_a: b>0,\ W_a: \bot$};
  \node[node, align=left] (s6) at (0, -5) {$R_a: b \geq 0,$\\$W_a: b<0 \lor b\geq 0$};
  \node[node] (s7) at (0, -6.5) {$finish$};
  
  \po{s0}{s2}{};\po{s2}{s4}{};
  \po{s6}{s7}{}; \po{s4}{s6}{};
  \sync{s0}{s1}{}; \sync{s1}{s2}{}; \sync{s4}{s5}{}; \sync{s5}{s7}{bend left=35};
\end{tikzpicture}
  \end{minipage}
  \caption{A data race in symbolic graph }
  \label{fig:graph}
\end{figure}

An illustration of algorithm\footnote{For clarity, we've displayed only access sets of \texttt{a}. } is given on Fig. \ref{fig:graph}. The first access to \texttt{a} cannot race with others since it happens before them according to \texttt{finish} semantics so corresponding graph nodes are ordered. Then, the node in the separate task has only read, while the node in the main thread has both read and writes. Omitting the read-read case, we check for the read-write race. It may occur with path conditions $b>0 \land b<0$ or $b>0 \land b\geq 0$. The first one is unsatisfiable, while the second one is satisfied with $b:=1$, so the program actually has a race. 

As a result, the extended algorithm not only establishes the possibility of a data race but also gives the exact input value which causes it. The state explosion problem is mitigated by merging same variable's accesses to the single node; that way, the conflict condition is expressed with one big constraint passed to the solver which may analyze it more efficiently. It's also possible to employ other optimizations, like on-the-fly constraint simplification --- for example, the write constraint in the last node could be reduced to $\top$. 

Currently, in our JPF implementation we consider only static symbolic graphs, i.e. those where task creation is independent of input values. It makes it easier to connect to the original algorithm since after backtracking to branching instruction no new vertices are added to the graph.

\section*{Future work}
Further development should go in various directions.

Firstly, the programs with conditional task creation should be supported.

Secondly, one may implement methods summarization to avoid executing the same code twice. 

Finally, performance comparison with other symbolic race detection algorithms is desirable as well. 

\bibliographystyle{plainurl}
\bibliography{sources}


\end{document}
