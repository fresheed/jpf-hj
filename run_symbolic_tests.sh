config=$1
case $config in
  symbolicGraph|symbolicFasttrack) echo "Using config: $config";;
  *) echo "Unknown config: $config"; exit 1 ;;
esac

./gradlew runSymbolic -Pconfig=$config
