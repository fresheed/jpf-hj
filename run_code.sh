./gradlew -PmainClass=benchmarks.symbolicComputationGraph.$1 exec
# awk 'NR==2{print "ranksep=0.1; nodesep=1;"}7' dots/$1.dot > tmp && mv tmp dots/$1.dot
dot -Tpng dots/$1.dot > dots/$1.png
#dot dots/$1.dot > dots/$1.png
#  ranksep=0.1;
#  nodesep=1;
